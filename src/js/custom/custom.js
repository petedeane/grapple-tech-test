/*------------------------------------*\
    CUSTOM JS
\*------------------------------------*/

$(function(){
    
    $('.js-journey-step').click(function(){
        var journeyStepID = $(this).parent().next('.journey-step').data('journey-step');

        $(this).parent().removeClass('is-active');
        $(this).parent().next('.journey-step').addClass('is-active');
        $('.js-journey-options-step').removeClass('is-active');

        $('.js-journey-options-step[data-journey-step = '+ journeyStepID +']').addClass('is-active');
    });

    $('.js-journey-options-step').click(function(){
        var journeyStepID = $(this).data('journey-step');
    
        if($(this).hasClass('is-active')){
            // Do nothing as the correct step should already be showing
        } else {
            $('.js-journey-options-step, .journey-step.is-active').removeClass('is-active');
            $(this).addClass('is-active');
            $('.journey-step[data-journey-step = '+ journeyStepID +']').addClass('is-active');
        }

        if($('.journey-final__results').hasClass('is-active')){
            $('.journey-final__results, .journey-final__map-wrapper').removeClass('is-active');
            $('.journey-options__option:last-child').removeClass('is-active');
        }
    });
        
    $('#journey-planner').submit(function(e) {
        e.preventDefault();

        // Get all the forms elements and their values in one step
        var values = $(this).serializeArray();

        // Fade out icons and empty results area ready for the input values
        $('.journey-options__output').not('.journey-options__output--ticket').each(function(){
            $(this).empty();
        });
        
        // Clear active states
        $('.journey-step, .journey-options, .js-journey-options-step, .journey-options__option').removeClass('is-active');
        $('.journey-options__option:last-child').addClass('is-active');

        // Get form values and place them in their new location

        setTimeout(function(){
            $('.journey-options__output--from').append(values[0].value);
            $('.journey-options__output--to').append(values[1].value);
            $('.journey-options__output--when').append(values[2].value + '<br>' + values[3].value);
            $('.journey-options__output--who').append(values[4].value + ' Adults');
            $('.journey-options__output--extras').append(values[5].value);
        }, 500);

        setTimeout(function(){
            $('.journey-final__results').addClass('is-active');
        }, 1000);

        $('.journey-final__map-wrapper').addClass('is-active');
    });
});