/*------------------------------------*\
    Localhost Path To Project
\*------------------------------------*/

var projectURL	= 'localhost/projects/grapple-tech-test';


/*------------------------------------*\
    File Paths
\*------------------------------------*/

var srcFolder           = 'src/'
var buildFolder         = 'build/';

var styleSRC            = 'src/scss/**/*.scss';
var styleBuild          = 'build/css';

var jsVendorSRC         = 'src/js/libs/*.js'; 
var jsVendorBuild       = 'build/js/';

var jsCustomSRC         = 'src/js/custom/*.js';
var jsCustomBuild       = 'build/js/';

var imagesSRC           = 'images/src/**/*.{png,jpg,gif,svg}';
var imagesBuild  		= 'images/opt/'; 

var htmlSRC    		    = './**/*.html';


/*------------------------------------*\
    Load Plugins
\*------------------------------------*/

var gulp         = require('gulp');
var sass         = require('gulp-sass');
var cleanCSS     = require('gulp-clean-css');
var del          = require('del');
var autoprefixer = require('gulp-autoprefixer');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var imagemin     = require('gulp-imagemin');
var rename       = require('gulp-rename');
var sourcemaps   = require('gulp-sourcemaps');
var notify       = require('gulp-notify');
var jshint       = require('gulp-jshint');
var browsersync  = require('browser-sync').create();

// Mac only
// var bsReuseTab   = require('browser-sync-reuse-tab')(browsersync, 'local');

var reload 		 = browsersync.reload;


/*------------------------------------*\
    SCSS Compilation
\*------------------------------------*/

gulp.task('styles', function () {
	return gulp.src(styleSRC)
  	.pipe( sourcemaps.init() )
    .pipe( sass()
    .on('error', sass.logError))
    .pipe( autoprefixer({
        browsers: ['last 5 versions'],
        cascade: false
    }))
    .pipe( cleanCSS({compatibility: 'ie11'}))
    .pipe( sourcemaps.write())
    .pipe( gulp.dest(styleBuild))
    .pipe( browsersync.stream())
    .pipe( notify( { message: 'TASK: "styles" Completed! 💯', onLast: true }))
});


/*----------------------------------------*\
    JS Custom Concat and Minify
\*----------------------------------------*/

gulp.task( 'customJS', ['jshint'], function() {
	gulp.src( jsCustomSRC )
	.pipe( concat( 'custom.js' ))
    .pipe( uglify())
    .pipe( gulp.dest(jsCustomBuild))
    .pipe( browsersync.stream())
    .pipe( notify( { message: 'TASK: "customJS" Completed! 💯', onLast: true }))
});


/*----------------------------------------*\
    JS Hint
\*----------------------------------------*/
gulp.task('jshint', function () {
    return gulp.src(jsCustomSRC)
    .pipe(jshint())
    .pipe(notify(function (file) {
		if (file.jshint.success) {
			return false;
		}

		var errors = file.jshint.results.map(function (data) {
			if (data.error) {
				return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
			}
		}).join("\n");
		return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
    }))

    .pipe(jshint.reporter('jshint-stylish', {beep: true}))
    .pipe(jshint.reporter('fail'))
});



/*------------------------------------*\
    JS Vendor Concat and Minify
\*------------------------------------*/

gulp.task( 'vendorJS', function() {
    gulp.src( jsVendorSRC )
	.pipe( concat( 'libs.js' ) )
	.pipe( uglify() )
	.pipe( gulp.dest( jsVendorBuild ))
	.pipe( browsersync.stream())
	.pipe( notify( { message: 'TASK: "vendorJS" Completed! 💯', onLast: true }))
});


/*------------------------------------*\
    Image minification
\*------------------------------------*/

gulp.task( 'imagemin', function() {
	gulp.src( imagesSRC )
	.pipe( imagemin( {
		  progressive: true,
		  optimizationLevel: 3,
		  interlaced: true,
		  svgoPlugins: [{removeViewBox: false}]
		} ) )
	.pipe( gulp.dest( imagesBuild ))
	.pipe( browsersync.stream())
	.pipe( notify( { message: 'TASK: "imagemin" Completed! 💯', onLast: true }))
});


/*------------------------------------*\
    BrowserSync Set Up
\*------------------------------------*/

// Mac only - opens browsersync and reuses tab every time gulp is restarted
// gulp.task('browser-sync', function() {
//     browsersync.init({
// 		proxy: projectURL,
// 		open: false,
// 		injectChanges: true
// 	}, bsReuseTab);
// });

// All OS - Opens browsersync in a new tab
gulp.task('browser-sync', function() {
    browsersync.init({
        proxy: projectURL,
        open: true,
        injectChanges: true
    });
});


gulp.task('reload', function () {
  browsersync.reload();
});


/*------------------------------------*\
    Empty Build folder
\*------------------------------------*/

gulp.task('clean', function() {
  return del(buildFolder);
});


/*------------------------------------*\
    Watch task
\*------------------------------------*/
gulp.task('watch', function() {
  gulp.watch( styleSRC, ['styles']);
  gulp.watch( jsCustomSRC , ['customJS']);
  gulp.watch( jsVendorSRC , ['vendorJS']);
  gulp.watch( imagesSRC , ['imagemin'], ['reload']);
  gulp.watch( htmlSRC , ['reload']);
});



/*------------------------------------*\
    Default task
\*------------------------------------*/

gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'customJS', 'vendorJS', 'imagemin', 'browser-sync', 'watch');
});
